<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mh_user".
 *
 * @property int $user_id รหัสผู้ใช้งาน
 * @property string $user_name ชื่อ
 * @property string $user_password รหัสผ่าน
 * @property string $user_firstname ชื่อ
 * @property string $user_lastname นามสกุล
 * @property string $user_tel1 เบอร์ติดต่อ 1
 * @property string $user_tel2 เบอร์ติดต่อ 2
 * @property string $user_line_id Line Id
 * @property string $user_line_ad Line @
 * @property string $user_fb_id facebook_id
 * @property string $user_fb_url Facebook ส่วนตัว URL
 * @property string $user_messenger Messenger
 * @property string $user_email Email
 * @property string $user_website Website
 * @property string $user_other อื่น ๆ
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 *
 * @property MhArchitectEngineer[] $mhArchitectEngineers
 * @property MhConcrete[] $mhConcretes
 * @property MhConstruction[] $mhConstructions
 * @property MhJob[] $mhJobs
 * @property MhStore[] $mhStores
 */
class MhUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_fb_id', 'updated_id'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['user_name', 'user_firstname', 'user_lastname', 'user_fb_url', 'user_messenger', 'user_email', 'user_website'], 'string', 'max' => 150],
            [['user_password', 'user_other'], 'string', 'max' => 255],
            [['user_tel1', 'user_tel2'], 'string', 'max' => 15],
            [['user_line_id', 'user_line_ad'], 'string', 'max' => 50],
            [['user_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'user_name' => 'User Name',
            'user_password' => 'User Password',
            'user_firstname' => 'User Firstname',
            'user_lastname' => 'User Lastname',
            'user_tel1' => 'User Tel1',
            'user_tel2' => 'User Tel2',
            'user_line_id' => 'User Line ID',
            'user_line_ad' => 'User Line Ad',
            'user_fb_id' => 'User Fb ID',
            'user_fb_url' => 'User Fb Url',
            'user_messenger' => 'User Messenger',
            'user_email' => 'User Email',
            'user_website' => 'User Website',
            'user_other' => 'User Other',
            'updated_id' => 'Updated ID',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhArchitectEngineers()
    {
        return $this->hasMany(MhArchitectEngineer::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhConcretes()
    {
        return $this->hasMany(MhConcrete::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhConstructions()
    {
        return $this->hasMany(MhConstruction::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhJobs()
    {
        return $this->hasMany(MhJob::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhStores()
    {
        return $this->hasMany(MhStore::className(), ['user_id' => 'user_id']);
    }
}
