<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mh_district".
 *
 * @property int $district_id รหัสตำบล
 * @property int $district_zip_code รหัสไปรษณีย์
 * @property string $district_name_th ชื่อตำบลภาษาไทย
 * @property string $district_name_en ชื่อตำบลภาษาอังกฤษ
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 * @property int $amphoe_id รหัสอำเภอ
 *
 * @property MhConcrete[] $mhConcretes
 * @property MhAmphoe $amphoe
 */
class MhDistrict extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_district';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['district_zip_code', 'district_name_th', 'district_name_en', 'amphoe_id'], 'required'],
            [['district_zip_code', 'updated_id', 'amphoe_id'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['district_name_th', 'district_name_en'], 'string', 'max' => 100],
            [['amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['amphoe_id' => 'amphoe_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'district_id' => 'District ID',
            'district_zip_code' => 'District Zip Code',
            'district_name_th' => 'District Name Th',
            'district_name_en' => 'District Name En',
            'updated_id' => 'Updated ID',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'amphoe_id' => 'Amphoe ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhConcretes()
    {
        return $this->hasMany(MhConcrete::className(), ['con_district_id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'amphoe_id']);
    }
}
