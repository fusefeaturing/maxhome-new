<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mh_amphoe".
 *
 * @property int $amphoe_id รหัสอำเภอ
 * @property string $amphoe_name_th ชื่ออำเภอภาษาไทย
 * @property string $amphoe_name_en ชื่ออำเภอภาษาอังกฤษ
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 * @property int $province_id รหัสจังหวัด
 *
 * @property MhProvince $province
 * @property MhArchitectEngineer[] $mhArchitectEngineers
 * @property MhConcrete[] $mhConcretes
 * @property MhConstruction[] $mhConstructions
 * @property MhDistrict[] $mhDistricts
 * @property MhJob[] $mhJobs
 * @property MhStore[] $mhStores
 */
class MhAmphoe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_amphoe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amphoe_name_th', 'amphoe_name_en', 'province_id'], 'required'],
            [['updated_id', 'province_id'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['amphoe_name_th', 'amphoe_name_en'], 'string', 'max' => 150],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['province_id' => 'province_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'amphoe_id' => 'Amphoe ID',
            'amphoe_name_th' => 'Amphoe Name Th',
            'amphoe_name_en' => 'Amphoe Name En',
            'updated_id' => 'Updated ID',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'province_id' => 'Province ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhArchitectEngineers()
    {
        return $this->hasMany(MhArchitectEngineer::className(), ['ae_amphoe_id' => 'amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhConcretes()
    {
        return $this->hasMany(MhConcrete::className(), ['con_amphoe_id' => 'amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhConstructions()
    {
        return $this->hasMany(MhConstruction::className(), ['cons_amphoe_id' => 'amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhDistricts()
    {
        return $this->hasMany(MhDistrict::className(), ['amphoe_id' => 'amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhJobs()
    {
        return $this->hasMany(MhJob::className(), ['job_amphoe_id' => 'amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhStores()
    {
        return $this->hasMany(MhStore::className(), ['store_amphoe_id' => 'amphoe_id']);
    }
}
