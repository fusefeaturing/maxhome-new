<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mh_concrete".
 *
 * @property int $con_id รหัสรายละเอียดคอนกรีต
 * @property int $user_id ชื่อผู้ใช้
 * @property string $con_desciption_product รายละเอียดคอนกรีต
 * @property int $con_strength กำลังแรงอัด
 * @property int $con_volume ปริมาณ
 * @property string $con_unit หน่วย
 * @property int $con_cube Cube
 * @property int $con_cylender Cylender
 * @property string $con_gps_transpot พิกัด GPS
 * @property int $con_province_id จังหวัด
 * @property int $con_amphoe_id อำเภอ
 * @property int $con_district_id ตำบล
 * @property string $con_pic_map รูปแผนที่
 * @property string $con_datetime วันที่
 * @property string $con_duration ช่วงเวลา เช้า บ่าย
 * @property string $con_name_contact con_name_contact
 * @property int $con_tel_contact con_tel_contact
 * @property string $con_other con_other
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 *
 * @property MhProvince $conProvince
 * @property MhAmphoe $conAmphoe
 * @property MhDistrict $conDistrict
 * @property MhUser $user
 */
class MhConcrete extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_concrete';
    }

    public $upload_foler = 'uploads';
    public $picture = 'nonee.png';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'con_strength', 'con_volume', 'con_cube', 'con_cylender', 'con_province_id', 'con_amphoe_id', 'con_district_id', 'con_tel_contact', 'updated_id'], 'integer'],
            [['con_strength', 'con_volume', 'con_datetime', 'con_duration', 'con_name_contact', 'con_tel_contact'], 'required'],
            [['con_unit', 'con_duration'], 'string'],
            [['con_datetime', 'created_time', 'updated_time'], 'safe'],
            [['con_desciption_product', 'con_gps_transpot', 'con_other'], 'string', 'max' => 255],
            [['con_name_contact'], 'string', 'max' => 150],
            [['con_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['con_province_id' => 'province_id']],
            [['con_amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['con_amphoe_id' => 'amphoe_id']],
            [['con_district_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhDistrict::className(), 'targetAttribute' => ['con_district_id' => 'district_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhUser::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [
                ['con_pic_map'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => 5,
                'extensions' => 'png,jpg,jpeg,gif',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'con_id' => 'รหัสรายละเอียดคอนกรีต',
            'user_id' => 'ชื่อผู้ใช้งาน',
            'con_desciption_product' => 'รายละเอียดคอนกรีต',
            'con_strength' => 'กำลังแรงอัด',
            'con_volume' => 'ปริมาณ',
            'con_unit' => 'หน่วย',
            'con_cube' => 'Cube',
            'con_cylender' => 'Cylender',
            'con_gps_transpot' => 'พิกัด GPS',
            'con_province_id' => 'จังหวัด',
            'con_amphoe_id' => 'อำเภอ',
            'con_district_id' => 'ตำบล',
            'con_pic_map' => Yii::t('app', 'รูปแผนที่'),
            'con_datetime' => 'วันที่ต้องการส่ง',
            'con_duration' => 'ช่วงเวลาที่ต้องการส่ง',
            'con_name_contact' => 'ชื่อติดต่อ',
            'con_tel_contact' => 'เบอร์โทรศัพท์',
            'con_other' => 'อื่นๆ',
            'updated_id' => 'รหัสเวลาแก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'con_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'con_amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConDistrict()
    {
        return $this->hasOne(MhDistrict::className(), ['district_id' => 'con_district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MhUser::className(), ['user_id' => 'user_id']);
    }

    public function pickup()
    {
        $result = '';
        if (($this->con_province_id != null) && ($this->con_province_id != '')) {
            $resultcoll = MhProvince::find()
                ->where(['province_id' => $this->con_province_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->province_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function deliver()
    {
        $result = '';
        if (($this->con_amphoe_id != null) && ($this->con_amphoe_id != '')) {
            $resultcoll = MhAmphoe::find()
                ->where(['amphoe_id' => $this->con_amphoe_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->amphoe_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewer()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrl() . $this->picture;
    }

    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerback()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrlback() . $this->picture;
    }




    public function uploadMultiple($model, $attribute)
    {
        $photos  = UploadedFile::getInstances($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $photos !== null) {
            $filenames = [];
            foreach ($photos as $file) {
                $filename = md5($file->baseName . time()) . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $filenames[] = $filename;
                }
            }

            if ($model->isNewRecord) {
                return implode(',', $filenames);
            } else {
                if (sizeof($filenames) == 0) {
                    return implode(',', (ArrayHelper::merge($filenames, $model->getOwnPhotosToArray())));
                } else {
                    return implode(',', $filenames);
                }
            }
        }

        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getPhotosViewer()
    {
        $photos = $this->con_pic_map ? @explode(',', $this->con_pic_map) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrl() . $photo, ['class' => 'img-thumbnail', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getPhotosViewerback()
    {
        $photos = $this->con_pic_map ? @explode(',', $this->con_pic_map) : [];
        $img = '';
/*
        var_dump($photos);
        die();
*/
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrlback() . $photo, ['class' => 'img-thumbnail', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotos()
    {
        $con_pic_map = $this->con_pic_map ? @explode(',', $this->con_pic_map) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        if (isset($con_pic_map[0])) {
            $img = Html::img($this->getUploadUrl() . $con_pic_map[0], ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotoURL()
    {
        $con_pic_map = $this->con_pic_map ? @explode(',', $this->con_pic_map) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($con_pic_map[0])) {
            $img = $this->getUploadUrl() . $con_pic_map[0];
        }
        return $img;
    }


    public function getFirstPhotosback()
    {
        $con_pic_map = $this->con_pic_map ? @explode(',', $this->con_pic_map) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        if (isset($con_pic_map[0])) {
            $img = Html::img($this->getUploadUrlback() . $con_pic_map[0], ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotoURLback()
    {
        $con_pic_map = $this->con_pic_map ? @explode(',', $this->con_pic_map) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($con_pic_map[0])) {
            $img = $this->getUploadUrlback() . $con_pic_map[0];
        }
        return $img;
    }


    public function getOwnPhotosToArray()
    {
        return $this->getOldAttribute('con_pic_map') ? @explode(',', $this->getOldAttribute('con_pic_map')) : [];
    }








    public function location_province($province)
    {
        $result = '';

        if ($province != null) {
            $pv = MhProvince::find()
                ->where(['province_id' => $province])
                ->one();

            if ($pv) {
                $result .= $pv->province_name_th;
            }
        }

        return $result;
    }

    public function location_amphoe($amphoe)
    {
        $result = '';


        if ($amphoe != null) {
            $am = MhAmphoe::find()
                ->where(['amphoe_id' => $amphoe])
                ->one();

            if ($am) {
                $result .= ' ' . $am->amphoe_name_th;
            }
        }


        return $result;
    }

    public function location_district($district)
    {
        $result = '';


        if ($district != null) {
            $dt = MhDistrict::find()
                ->where(['district_id' => $district])
                ->one();

            if ($dt) {
                $result .= ' ' . $dt->district_name_th;
            }
        }


        return $result;
    }
}
