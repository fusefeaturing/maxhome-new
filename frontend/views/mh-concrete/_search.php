<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MhConcreteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-concrete-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'con_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'con_desciption_product') ?>

    <?= $form->field($model, 'con_strength') ?>

    <?= $form->field($model, 'con_volume') ?>

    <?php // echo $form->field($model, 'con_unit') ?>

    <?php // echo $form->field($model, 'con_cube') ?>

    <?php // echo $form->field($model, 'con_cylender') ?>

    <?php // echo $form->field($model, 'con_gps_transpot') ?>

    <?php // echo $form->field($model, 'con_province_id') ?>

    <?php // echo $form->field($model, 'con_amphoe_id') ?>

    <?php // echo $form->field($model, 'con_district_id') ?>

    <?php // echo $form->field($model, 'con_pic_map') ?>

    <?php // echo $form->field($model, 'con_datetime') ?>

    <?php // echo $form->field($model, 'con_duration') ?>

    <?php // echo $form->field($model, 'con_name_contact') ?>

    <?php // echo $form->field($model, 'con_tel_contact') ?>

    <?php // echo $form->field($model, 'con_other') ?>

    <?php // echo $form->field($model, 'updated_id') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
