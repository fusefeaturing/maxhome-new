<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConcrete */

$this->title = 'สั่งคอนกรีต';

//$this->params['breadcrumbs'][] = ['label' => 'คอนกรีตของฉัน', 'url' => ['mycon']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-concrete-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
        'district' => [],
    ]) ?>

</div>
