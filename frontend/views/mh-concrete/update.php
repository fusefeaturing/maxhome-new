<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConcrete */
$userid = yii::$app->user->identity->id;
$this->title = 'แก้ไขข้อมูลคอนกรีต ' ;
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'รายการคอนกรีต', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'ประวัติการสั่งคอนกรีตของฉัน', 'url' => ['mycon'], 'id' => $userid];

}
//$this->params['breadcrumbs'][] = ['label' => 'คอนกรีตของฉัน', 'url' => ['mycon']];
$this->params['breadcrumbs'][] = ['label' => $model->con_desciption_product, 'url' => ['view', 'id' => $model->con_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-concrete-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
        'district' => $district,
    ]) ?>

</div>
