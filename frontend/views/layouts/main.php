<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;


$currentUrl = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; //Url::canonical();
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <script>



btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'headerContent' => Html::a('089-519-7777','tel:+66618728888',['class' => 'navbar-brand navbar-link']),
        
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        //Html::a('กลับไปที่ maxhome.co',"https://line.me/R/ti/p/%40maxhome"),
        //['label' => 'หน้าแรก', 'url' => ['https://maxhome.co/']],
        ['label' => 'หาช่างรับเหมาก่อสร้าง', 'url' => ['/mh-job/index']],
        ['label' => 'หาร้านวัสดุก่อสร้าง', 'url' => ['/mh-construction/index']],
        //['label' => 'ดูบทความ', 'url' => ['/site/contact']],
        
        //['label' => 'ดูสินค้า', 'url' => ['/mh-construction/index']],
        ['label' => 'หาสถาปนิก / วิศวกร', 'url' => ['/mh-architect-engineer/index']],
        ['label' => 'สั่งคอนกรีต', 'url' => ['/mh-concrete/create']],
        
    ];
    if (Yii::$app->user->isGuest) {
        
        $menuItems[] = ['label' => 'เข้าสู่ระบบ', 'url' => ['/site/login']];
    } else {
        $menuItems[] = ['label' => 'แก้ไขข้อมูลส่วนตัว', 'url' => ['/mh-user/profile']];
        
        
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'ออกจากระบบ (' . Yii::$app->user->identity->user_firstname . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container" style="padding-top: 110px;">
    
    <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-arrow-circle-up"></i></button>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>


<nav class="navbar-default navbar-fixed-bottom">
            <div class="container-fluid">

                <div class="row ">
                         
                <div id="wpfront-notification-bar" class="wpfront-fixed ">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <div class="wpfront-message">ต้องการความช่วยเหลือติดต่อสอบถาม ทีมงาน MaxHome</div>
                    <div> 
                        <a class="wpfront-button" href="https://line.me/R/ti/p/%40maxhome"  target="_blank" >LINE ID : @MaxHome</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>      
                    
                </div>
        </nav>


<!--<footer class="footer">
<div id="wpfront-notification-bar" class="wpfront-fixed ">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <div class="wpfront-message"> โปรโมชั่น ส่วนลดพิเศษ ติดต่อทีมขาย</div>
                    <div> 
                        <a class="wpfront-button" href="https://line.me/R/ti/p/%40maxhome"  target="_blank" >LINE ID : @MaxHome</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>



</footer>-->

<?php $this->endBody() ?>
<script>
//Get the button:
mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
</script>


</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
</html>
<?php $this->endPage() ?>
