<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhConcrete;

/**
 * MhConcreteSearch represents the model behind the search form of `common\models\MhConcrete`.
 */
class MhConcreteSearch extends MhConcrete
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['con_id', 'user_id', 'con_volume', 'con_cube', 'con_cylender', 'con_province_id', 'con_amphoe_id', 'con_district_id', 'updated_id'], 'integer'],
            [['con_desciption_product', 'con_strength', 'con_unit', 'con_gps_transpot', 'con_pic_map', 'con_datetime', 'con_duration', 'con_name_contact', 'con_tel_contact', 'con_other', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhConcrete::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'con_id' => $this->con_id,
            'user_id' => $this->user_id,
            'con_volume' => $this->con_volume,
            'con_cube' => $this->con_cube,
            'con_cylender' => $this->con_cylender,
            'con_province_id' => $this->con_province_id,
            'con_amphoe_id' => $this->con_amphoe_id,
            'con_district_id' => $this->con_district_id,
            'con_datetime' => $this->con_datetime,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'con_desciption_product', $this->con_desciption_product])
            ->andFilterWhere(['like', 'con_strength', $this->con_strength])
            ->andFilterWhere(['like', 'con_unit', $this->con_unit])
            ->andFilterWhere(['like', 'con_gps_transpot', $this->con_gps_transpot])
            ->andFilterWhere(['like', 'con_pic_map', $this->con_pic_map])
            ->andFilterWhere(['like', 'con_duration', $this->con_duration])
            ->andFilterWhere(['like', 'con_name_contact', $this->con_name_contact])
            ->andFilterWhere(['like', 'con_tel_contact', $this->con_tel_contact])
            ->andFilterWhere(['like', 'con_other', $this->con_other]);

        return $dataProvider;
    }
}
