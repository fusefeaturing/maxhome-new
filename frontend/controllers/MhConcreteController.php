<?php

namespace frontend\controllers;

use Yii;
use common\models\MhConcrete;
use backend\models\MhConcreteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\MhAmphoe;
use common\models\MhDistrict;

/**
 * MhConcreteController implements the CRUD actions for MhConcrete model.
 */
class MhConcreteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MhConcrete models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $searchModel = new MhConcreteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view_all_conlist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    

    public function actionMycon()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->identity;

        $searchModel = new MhConcreteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
        ->query
        ->where(['in', 'user_id', $user_id]);

        return $this->render('my_con', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            
        ]);
}

    /**
     * Displays a single MhConcrete model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $owner = false;

        $user_id = Yii::$app->user->id;
        if ($model->user_id == $user_id) {
            $owner = true;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'owner' => $owner,
        ]);
    }

    /**
     * Creates a new MhConcrete model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /*if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }*/
        $model = new MhConcrete();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $model->con_pic_map = $model->uploadMultiple($model, 'con_pic_map');               
                
            }

            if(Yii::$app->request->isPost) {
                
                    $con_desciption_product = Yii::$app->request->post('MhConcrete[con_desciption_product]');
                    Yii::$app->mailer->compose()
                    ->setTo('fusefeaturing@gmail.com')
                    ->setFrom('fusefeaturing@gmail.com')
                    ->setSubject('ทดสอบ')
                    ->setHtmlBody('<b>'.$con_desciption_product.'</b>')
                    ->setBcc('fusefeaturing@gmail.com')
                    
                    ->send();
        
                  
                  
            }

            if ($model->save()) {
                //return $this->redirect(['view', 'id' => $model->job_id]);
                return $this->redirect(['view', 'id' => $model->con_id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MhConcrete model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        $amphoe = ArrayHelper::map($this->getAmphoe($model->con_amphoe_id), 'id', 'name');
        $district = ArrayHelper::map($this->getDistrict($model->con_district_id), 'id', 'name');

        if ($model->load(Yii::$app->request->post())) {

            
            if ($model->validate()) {
                $model->con_pic_map = $model->uploadMultiple($model, 'con_pic_map');
            }

            if ($model->save()) {
                //return $this->redirect(['view', 'id' => $model->job_id]);
                return $this->redirect(['view', 'id' => $model->con_id]);
            }

        }

        return $this->render('update', [
            'model' => $model,
            'amphoe' => $amphoe,
            'district' => $district,
        ]);
    }

    /**
     * Deletes an existing MhConcrete model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MhConcrete model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhConcrete the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhConcrete::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetAmphoe()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphoe($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetDistrict()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $province_id = empty($ids[0]) ? null : $ids[0];
            $amphoe_id = empty($ids[1]) ? null : $ids[1];
            if ($province_id != null) {
                $data = $this->getDistrict($amphoe_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getAmphoe($id)
    {
        $datas = MhAmphoe::find()->where(['province_id' => $id])->all();
        return $this->MapData($datas, 'amphoe_id', 'amphoe_name_th');
    }

    protected function getDistrict($id)
    {
        $datas = MhDistrict::find()->where(['amphoe_id' => $id])->all();
        return $this->MapData($datas, 'district_id', 'district_name_th');
    }

    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }


}
